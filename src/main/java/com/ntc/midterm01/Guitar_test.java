/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.midterm01;

/**
 *
 * @author L4ZY
 */
public class Guitar_test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Guitar guitar01 = new Guitar("Guitar01", "Yamaha", "nylon", "Parlour", 6, 'Y', 'N');
        guitar01.checkGuitar_type();//เช็คชนิดของกีตาร์ และปริ้น คุณสมบัติ
        guitar01.play();

        Guitar guitar02 = new Guitar("Guitar02", "Martin", "steel", "O", 6, 'N', 'N');
        guitar02.checkGuitar_type();
        guitar02.play();

        guitar02.guitartuned();
        guitar02.play();

        Guitar guitar03 = new Guitar("Guitar03", "Gibson", "steel", "Stratocaster", 6, 'Y', 'Y');
        guitar03.checkGuitar_type();
        guitar03.play();
        
        System.out.println("Complete");
                
 
        
    }

}
