/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.midterm01;

/**
 *
 * @author L4ZY
 */
public class Guitar {

    private String name, Brand, TypeofString, Model;
    private int NumberofString;
    private char tuned, electric;

    public Guitar(String name, String Brand, String TypeofString, String Model,
            int NumberofString, char tuned, char electric) {
        this.name = name;
        this.Brand = Brand;
        this.TypeofString = TypeofString;
        this.Model = Model;
        this.NumberofString = NumberofString;
        this.tuned = tuned;
        this.electric = electric;
        //ใส่ชื่อ,แบรนด์,ชนิดของสาย,โมเดล,จำนวนสาย,จูน,ใช้ไฟฟ้า

    }

    public void checkGuitar_type() {//เช็คชนิดของกีตาร์ และปริ้น คุณสมบัติ
        if (electric == 'Y') {
            System.out.println("this " + this.name + " is Electric Guitar " + Brand + " Brand " + "\n" + Model + " Model "
                    + " and have " + NumberofString + " " + TypeofString + " String");

        } else if (TypeofString.equals("nylon")) {
            System.out.println("this " + this.name + " is Classical Guitar " + Brand + " Brand " + "\n" + Model + " Model "
                    + " and have " + NumberofString + " " + TypeofString + " String");
        } else {
            System.out.println("this " + this.name + " is Acoustic Guitar " + Brand + " Brand " + "\n" + Model + " Model "
                    + " and have " + NumberofString + " " + TypeofString + " String");

        }

    }

    public boolean isTuned() {//เช็คสถานะว่าจูนหรือยัง
        if (tuned == 'Y') {
            return true;
        }

        return false;
    }

    public void guitartuned() {//จูนสายกีตาร์
        this.tuned = 'Y';
    }

    public void play() {
        if (isTuned() == true) {
            System.out.println(this.name + " can play ~~~~~~");
            line();
        } else {
            System.out.println(this.name + " can't play because it's not tuned XXXXXX");
            line();
        }

    }

    public void line() {
        System.out.println("------------------------------------");
    }

}
